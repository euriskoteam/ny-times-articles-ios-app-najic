# NY Times Most Popular Articles iOS App

## This app is built to hit the NY Times Most Popular Articles API and show a list of articles, that shows details when items on the list are tapped.

## This app is using the below pods as dependencies
  - ReachabilitySwift: For network state change detection
  - Fabric & Crashlytics: For crash reporting
  - Kingfisher: For a smooth way to load and cash Images 

## Main features
  - Green themed app listing the mostpopular & mostviewed news.
  - Ability to search locally for news by title or by abstract.
  - Ability to change news data source by simply switching Period enum.
  - Ability to change image format (quality) by simply switching MetaDataFormat enum.
  
In addition you can:
  - Change the news data source by going to 'NewsExtension.Swift'
  - Find "api.fetchMostViewedNews(section: SectionPath.all, period: Period.one)"
  - Change the 'section:' and/or 'period:' enums to get data as your needs
  
# UniTests
- XCTest:  To test API call for fetching most viewed news
- XCUITest: To test News View Controller UI