//
//  APIEndPoints.swift
//  NYTimes
//
//  Created by Eurisko on 2/12/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

// MARK API Endpoints
struct APIEndPoints {
    static let fetchMostViewedNews = "/svc/mostpopular/v2/mostviewed"
}
