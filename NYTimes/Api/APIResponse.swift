//
//  APIResponse.swift
//  NYTimes
//
//  Created by Eurisko on 2/12/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import Foundation

// MARK API Response Status
enum ResponseStatus: Int {
    /* CHANGE IT TO EXACT CODE USED IN YOUR PROJECT */
    case success = 1
    case failure = 0
    case conenctionTimeout = -1
}

// MARK API Response Message
struct ResponseMessage {
    static let serverUnreachable: String = translate(key: "serverUnreachable")
    static let connectionTimeout: String = translate(key: "connectionTimeout")
}

// MARK API Response Data
struct ResponseData {
    var status: Int = ResponseStatus.success.rawValue
    var message: String = String()
    var data: Data? = nil
    
    init() {}
}
