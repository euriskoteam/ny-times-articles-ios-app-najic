//
//  APIManager.swift
//  NYTimes
//
//  Created by Eurisko on 2/12/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import Foundation

// MARK API HTTP Parameters
typealias Parameters = [String: Any]

// MARK API HTTP Headers
typealias HTTPHeaders = [String: Any]

// MARK API CompletionHandler
typealias CompletionHandler = (_ response: ResponseData) -> Void

// API Manager
struct APIManager {        
    // MARK API Manager Shared Instance
    static let shared = APIManager.init()
    
    // MARK API Private Init
    private init() {}
    
    // MARK: API SERVER REQUEST
    private func makeHttpRequest(endPoint: String, httpMethod: HTTPMethod, parameters: Parameters? = nil, headers: HTTPHeaders? = nil, completionHandler: @escaping CompletionHandler) {
        var responseData = ResponseData()
        
        let urlString = "\(coreapi.BaseUrl)\(endPoint)?api-key=\(coreapi.APIKey)"
        
        guard let url = URL(string: urlString) else {
            responseData.status = ResponseStatus.failure.rawValue
            responseData.message = ResponseMessage.serverUnreachable
            completionHandler(responseData)
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = httpMethod.rawValue
        
        if let httpHeaders = headers as? [String : String] {
            urlRequest.allHTTPHeaderFields = httpHeaders
        }
        
        if let httpBody = parameters {
            do {
                urlRequest.httpBody = try NSKeyedArchiver.archivedData(withRootObject: httpBody, requiringSecureCoding: false)
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        
        let task = coreapi.manager.dataTask(with: urlRequest) {
            (data, response, error) in

            guard error == nil else {
                responseData.status = ResponseStatus.failure.rawValue
                responseData.message = error?.localizedDescription ?? ResponseMessage.serverUnreachable
                completionHandler(responseData)
                return
            }

            guard let data = data else {
                responseData.status = ResponseStatus.failure.rawValue
                responseData.message = ResponseMessage.serverUnreachable
                completionHandler(responseData)
                return
            }
            
            responseData.data = data
            completionHandler(responseData)
        }
        task.resume()
    }
}

// MARK: API Manager Extension
extension APIManager {
    
    /******************** API Calls ********************/
    
    // MARK Fetch Most Viewed News
    func fetchMostViewedNews(section: SectionPath, period: Period, completionHandler: @escaping CompletionHandler) {
        
        let endPoint = "\(APIEndPoints.fetchMostViewedNews)\(section.rawValue)\(period.rawValue)"
        
        self.makeHttpRequest(endPoint: endPoint, httpMethod: .get, completionHandler: { response in
            completionHandler(response)
        })
    }
}
