//
//  News.swift
//  NYTimes
//
//  Created by Eurisko on 2/13/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import Foundation

struct News : Codable {
	let status : String?
	let copyright : String?
	let num_results : Int?
	var results : [Results]?

	enum CodingKeys: String, CodingKey {
		case status = "status"
		case copyright = "copyright"
		case num_results = "num_results"
		case results = "results"
	}
    
    init() {
        status = nil
        copyright = nil
        num_results = nil
        results = nil
    }

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		copyright = try values.decodeIfPresent(String.self, forKey: .copyright)
		num_results = try values.decodeIfPresent(Int.self, forKey: .num_results)
		results = try values.decodeIfPresent([Results].self, forKey: .results)
	}
}
