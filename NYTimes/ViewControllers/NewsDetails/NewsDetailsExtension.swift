//
//  NewsDetailsExtension.swift
//  NYTimes
//
//  Created by Eurisko on 2/13/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import Foundation

extension NewsDetailsViewController {
    func initializeViews() {
        if let media = self.newsDetails?.media?.first {
            let metaData = media.media_metadata?.first(
                where: { $0.format == MetaDataFormat.large.rawValue }
            )
            
            if let url = metaData?.url,
                let imageUrl = URL(string: url) {
                self.imageViewLarge.kf.setImage(with: imageUrl)
            }
        }
        
        self.labelTitle.text = self.newsDetails?.title
        self.labelDescription.text = self.newsDetails?.abstract
        self.labelDate.text = self.newsDetails?.published_date
    }
}
