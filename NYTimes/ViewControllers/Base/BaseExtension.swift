//
//  BaseExtension.swift
//  NYTimes
//
//  Created by Eurisko on 2/12/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import UIKit

extension BaseViewController {
    // MARK Global Methods
    
    // MARK Redirect To View Controller
    func redirectTo(storyboard: UIStoryboard, storyboardId: String, type: SegueType, animated: Bool = true) {
        let destinationViewController = storyboard.instantiateViewController(withIdentifier: storyboardId)
        switch type {
        case .push:
            self.navigationController?.pushViewController(destinationViewController, animated: true)
            break
        case .present:
            self.present(destinationViewController, animated: animated, completion: nil)
            break
        }
    }
    
    // MARK Show Alert
    func showAlert(title: String = Bundle.main.displayName, message: String, style: UIAlertController.Style, shouldPopVC: Bool = false) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { action in
            if shouldPopVC {
                self.pop()
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

    // MARK Dismiss Keyboard
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    // MARK Pop View Controller
    func pop(animated: Bool = true) {
        self.navigationController?.popViewController(animated: animated)
    }
    
    // MARK Dismiss View Controller
    func dismiss(animated: Bool = true) {
        self.dismiss(animated: animated, completion: nil)
    }
    
    // MARK Show Loader
    func showLoader(style: UIActivityIndicatorView.Style = .whiteLarge, color: UIColor = Color.appGreen) {
        self.activityIndicator.style = style
        self.activityIndicator.color = color
        self.view.addSubview(self.activityIndicator)
    }
    
    // MARK Hide Loader
    func hideLoader() {
        self.activityIndicator.removeFromSuperview()
    }
    
    // MARK Parse Codable
    func parseCodable <T : Codable> (data:Data?, type: T.Type, successHandler:@escaping (_ details: T) -> Void, errorHandler:@escaping (_ error:String) -> Void)  {
        
        guard let responseData = data, let decodedJson = try? JSONDecoder().decode(T.self, from: responseData) else {
            errorHandler("Parsing error, please contact support")
            return
        }
        successHandler(decodedJson)
    }
}
