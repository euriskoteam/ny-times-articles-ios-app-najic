//
//  NewsExtension.swift
//  NYTimes
//
//  Created by Eurisko on 2/13/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension NewsViewController {
    
    //MARK: - Add Refresh Control
    
    func addRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: translate(key: "pullToRefresh"))
        self.refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        self.tableView.addSubview(self.refreshControl)
    }
    
    @objc func refreshData() {
        self.fetchMostViewedNews()
    }
    
    //MARK: - Setup varws
    
    func setupViews() {
        self.title = translate(key: "newsTitle")
        
        self.setupButtonSearch()
    }
    
    func setupButtonSearch() {
        let buttonSearch = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(buttonSearchTapped))
        self.navigationItem.rightBarButtonItem = buttonSearch
    }
    
    func setupButtonDone() {
        let buttonSearch = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(buttonDoneTapped))
        self.navigationItem.rightBarButtonItem = buttonSearch
    }
    
    //MARK: - Button Search Tapped
    
    @objc func buttonSearchTapped() {
        self.setupButtonDone()
        
        self.searchBarTopConstraint.constant = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func buttonDoneTapped() {
        self.setupButtonSearch()
        
        self.searchBar.resignFirstResponder()
        self.searchBar.text = nil
        
        self.searchBarTopConstraint.constant = -self.searchBar.frame.height
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    //MARK: - Setup TableView
    
    func setupTableView() {
        self.tableView.register(UINib.init(nibName: CellIds.NewsTableViewCell, bundle: nil), forCellReuseIdentifier: CellIds.NewsTableViewCell)
        
        self.tableView.tableFooterView = UIView()
    }
    
    //MARK: - TableView DataSource Methods
    
    func getNumberOfSections() -> Int {
        return 1
    }
    
    func getNumberOfRowsInSection(section: Int) -> Int {
        return self.filteredNews.count
    }
    
    func getHeightForRowAt(indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func getCell(indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: CellIds.NewsTableViewCell) as? NewsTableViewCell {
            let newsItem = self.filteredNews[indexPath.row]
            
            //Label Title
            cell.labelTitle?.text = newsItem.title
            
            //Label By
            let byLine = newsItem.byline ?? ""
            let publishedDate = newsItem.published_date ?? ""
            let final = AppUtils.buildAttrStringFrom2Strings(string1Text: byLine + " " + translate(key: "on") + " ", string1Color: .lightGray, string1Font: UIFont.systemFont(ofSize: 15), string2Text: publishedDate, string2Color: .lightGray, string2Font: UIFont.systemFont(ofSize: 15))
            cell.labelBy?.attributedText = final
            
            //Image View
            if let media = newsItem.media?.first {
                let metaData = media.media_metadata?.first(where: { $0.format == MetaDataFormat.normal.rawValue })
                
                if let url = metaData?.url,
                    let imageUrl = URL(string: url) {
                    cell.imageViewIcon.kf.setImage(with: imageUrl)
                }
            }
            
            return cell
        }
 
        return UITableViewCell()
    }
    
    func fetchMostViewedNews() {
        self.showLoader()
        
        api.fetchMostViewedNews(section: SectionPath.all, period: Period.one, completionHandler: { success in
            guard let data = success.data else {
                return
            }
            
            self.parseCodable(data: data, type: News.self, successHandler: { news in
                
                //Save response in core data
                DispatchQueue.main.async {
                    if let results = news.results {
                        self.news = results
                        self.filteredNews = self.news
                    }
                    
                    self.refreshControl.endRefreshing()
                    self.tableView.reloadData()
                    self.hideLoader()
                    
                }
            }, errorHandler: { (error) in
                self.showAlert(message: error, style: .alert)
            })
        })
    }
    
    func reloadData() {
        self.filteredNews = self.news
        self.tableView.reloadData()
    }
}
