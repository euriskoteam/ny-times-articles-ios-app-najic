//
//  ViewExtension.swift
//  NYTimes
//
//  Created by Eurisko on 2/12/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import UIKit
import Foundation

extension UIView {
    func animate(withDuration duration: TimeInterval = 0.5, animation: Animation) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = animation == .fadeIn ? 1.0 : 0.0
        })
    }
    
    func loadAndSetupXib(fromNibNamed nibName: String) {
        if let view = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)?.first as? UIView {
            view.frame = self.bounds
            view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.addSubview(view)
        }
    }
    
    func customizeView(backgroundColor: UIColor, radius: CGFloat = 0) {
        self.backgroundColor = backgroundColor
        self.layer.cornerRadius = radius
    }
    
    func customizeBorder(color: UIColor, width: CGFloat) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }
    
    func rounded() {
        self.layer.cornerRadius = self.frame.height/2
    }
    
    func isEnabled(enable: Bool) {
        self.isUserInteractionEnabled = enable
        self.alpha = enable ? 1 : 0.5
    }
}
