//
//  TextFieldExtension.swift
//  NYTimes
//
//  Created by Eurisko on 2/12/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import UIKit
import Foundation

extension UITextField {
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
    
    func isNullOrEmpty() -> Bool {
        return self.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty ?? true
    }
}
