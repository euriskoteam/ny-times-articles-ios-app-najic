//
//  TextViewExtension.swift
//  NYTimes
//
//  Created by Eurisko on 2/12/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import UIKit
import Foundation

extension UITextView {
    func isNullOrEmpty() -> Bool {
        return self.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty ?? true
    }
}
