//
//  ImageViewExtension.swift
//  NYTimes
//
//  Created by Eurisko on 2/12/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import UIKit

extension UIImageView {
    func customizeTint(color: UIColor) {
        self.image = self.image?.withRenderingMode(.alwaysTemplate)
        self.tintColor = color
    }
}
