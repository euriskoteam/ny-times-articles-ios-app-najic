//
//  Constants.swift
//  NYTimes
//
//  Created by Eurisko on 2/12/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import Foundation
import UIKit

//******************** Public Usage ********************//

let appDelegate: AppDelegate? = {
    guard let _appDelegate = UIApplication.shared.delegate as? AppDelegate else {
        return nil
    }
    return _appDelegate
}()

let isDebug = true
let coreapi = APIConfig.shared
let api = APIManager.shared

//******************** End of Public Usage ********************//

public func translate(key:String) -> String {
    return NSLocalizedString(key, comment: "")
}

struct Constants {
    static let APPSTORE_ID = ""
}

//User Default
struct UserDefaultsKey {
    
}

//Keychain
struct KeychainKeys {
    
}

//In-app Notifications
struct NotificationsKeys {
    
}
