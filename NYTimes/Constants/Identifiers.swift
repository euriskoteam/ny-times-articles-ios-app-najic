//
//  Identifiers.swift
//  NYTimes
//
//  Created by Eurisko on 2/12/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import UIKit

struct StoryboardIds {
    static let NewsViewController: String = "NewsViewController"
    static let NewsDetailsViewController: String = "NewsDetailsViewController"
}

struct ViewIds {
    static let NotificationView: String = "NotificationView"
}

struct CellIds {
    static let NewsTableViewCell: String = "NewsTableViewCell"
}
