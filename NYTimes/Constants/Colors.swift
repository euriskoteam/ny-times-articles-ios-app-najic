//
//  Colors.swift
//  NYTimes
//
//  Created by Eurisko on 2/12/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import UIKit

struct Color {
    static let white: UIColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    static let appGreen: UIColor = UIColor(red: 70.0/255.0, green: 225.0/255.0, blue: 190.0/255.0, alpha: 1.0)
}
