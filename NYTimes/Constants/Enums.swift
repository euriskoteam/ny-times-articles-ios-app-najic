//
//  Enums.swift
//  NYTimes
//
//  Created by Eurisko on 2/12/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import UIKit

enum SegueType {
    case push
    case present
}

enum Animation {
    case fadeIn
    case fadeOut
}

enum JPEGQuality: CGFloat {
    case lowest = 0.0
    case low = 0.25
    case medium = 0.5
    case high = 0.75
    case highest = 1.0
}

enum MetaDataFormat: String {
    case thumb = "Standard Thumbnail"
    case normal = "Normal"
    case large = "Large"
    case jumbo = "Jumbo"
    case square320 = "square320"
    case square640 = "square640"
    case largeThumb = "Large Thumbnail"
    case medium210 = "mediumThreeByTwo210"
    case medium440 = "mediumThreeByTwo440"
}

enum Period: String {
    case seven = "/7.json"
    case one = "/1.json"
    case thirty = "/30.json"
}

enum SectionPath: String {
    case all = "/all-sections"
}
