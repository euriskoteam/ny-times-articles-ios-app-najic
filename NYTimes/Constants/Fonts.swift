//
//  Fonts.swift
//  NYTimes
//
//  Created by Eurisko on 2/12/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import UIKit

struct Font {
    static let names: [String?] = UIFont.fontNames(forFamilyName: "") // Add your font name
    
    static var textLight: UIFont {
        get {
            if let fontName = Font.names[0] {
                return UIFont.init(name: fontName, size: 16)!
            }
            
            return UIFont.init()
        }
    }
    
    static var textRegular: UIFont {
        get {
            if let fontName = Font.names[1] {
                return UIFont.init(name: fontName, size: 16)!
            }
            
            return UIFont.init()
        }
    }
    
    static var textBold: UIFont {
        get {
            if let fontName = Font.names[2] {
                return UIFont.init(name: fontName, size: 16)!
            }
            
            return UIFont.init()
        }
    }
}
