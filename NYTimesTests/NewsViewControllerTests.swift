//
//  NewsViewControllerTests.swift
//  NYTimesTests
//
//  Created by Eurisko on 2/14/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import XCTest
@testable import NYTimes

class NewsViewControllerTests: XCTestCase {
    
    var controllerToTest: NewsViewController?
    var sessionToTest: URLSession!
    
    let API_KEY: String = "Rk6GfVjL9XA3A5ipo7bjr2fNh80CpeA5"
    let BASE_URL: String = "http://api.nytimes.com/"
    let EndPoint: String = "svc/mostpopular/v2/mostviewed"
    let Section: String = "all-sections"
    let Period: String = "1.json"
    
    override func setUp() {
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let mainStoryboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        controllerToTest = mainStoryboard.instantiateViewController(withIdentifier: StoryboardIds.NewsViewController) as? NewsViewController ?? NewsViewController()
        sessionToTest = URLSession(configuration: URLSessionConfiguration.default)
    }
    
    override func tearDown() {
        
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        controllerToTest = nil
        sessionToTest = nil
        
        super.tearDown()
    }
    
    //MARK:- Test Functions
    
    func testMostViewedNewsApi() {
        
        // given
        let url = URL(string: "\(BASE_URL)\(EndPoint)\(Section)\(Period)?api-key=\(API_KEY)")
        
        // 1
        let promise = expectation(description: "Completion handler invoked")
        var statusCode: Int?
        var responseError: Error?
        
        // when
        let dataTask = sessionToTest.dataTask(with: url!) { data, response, error in
            statusCode = (response as? HTTPURLResponse)?.statusCode
            responseError = error
            // 2
            promise.fulfill()
        }
        dataTask.resume()
        // 3
        waitForExpectations(timeout: 10, handler: nil)
        
        // then
        XCTAssertNil(responseError) // Expect error to be nil and response data to be defined, otherwise the test will fail
        XCTAssertEqual(statusCode, 200) // Expect response status code 200 with data as bytes, otherwise the test will fail
    }
    
}
