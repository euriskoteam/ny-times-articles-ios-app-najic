//
//  NewsViewControllerUITests.swift
//  NYTimesUITests
//
//  Created by Eurisko on 2/14/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import XCTest

class NewsViewControllerUITests: XCTestCase {

    var controllerToTest: NewsViewController?

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        controllerToTest = nil
    }

    //MARK:- My Test Functions
    func testClickOnTheFirstCell() {

        let app = XCUIApplication()
        let firstChild = app.tables.children(matching:.any).element(boundBy: 0)
        waitFor(object: firstChild) { let found = $0.exists
            if found {
                $0.tap()
            }
            return found
        }
    }

    func waitFor<T>(object: T, timeout: TimeInterval = 10, file: String = #file, line: UInt = #line, expectationPredicate: @escaping (T) -> Bool) {
        let predicate = NSPredicate { obj, _ in
            expectationPredicate(obj as! T)
        }
        expectation(for: predicate, evaluatedWith: object, handler: nil)

        waitForExpectations(timeout: timeout) { error in
            if (error != nil) {
                let message = "Failed to fulful expectation block for \(object) after \(timeout) seconds."
                self.recordFailure(withDescription: message, inFile: file, atLine: Int(line), expected: true)
            }
        }
    }

}
